# Selezione e scaricamento di dati LiDAR (nuvole) per una specifica area geografica

La necessità può essere delineata in questo:

1. Visualizzazione di una mappap (usando delle tiles da un servizio GIS).
2. Disegnare un poligono che delimiti un'area sulla mappa.
3. Rilevare la dimensione reale dell'area delineata dal poligono.
4. Generare lo shape file dell'area selezionata.
5. Ottenere la nuvola LiDAR per scaricarla nel formato preferito: LAS o LAZ.

## Utilizzo di Leaflet per visualizzare la mappa

Creiamo una mappa del centro di una determinata zona geografica utilizzando le
tiles OpenStreetMap. Per prima cosa si inizializza la mappa e si imposta la sua
vista sulle coordinate prescelte e su uno specifico livello di zoom:

```javascript
    // use openstreetmap to display tiles
    var map = L.map('map').setView([41.764611, 12.372389], 13);
```

Per impostazione predefinita (poiché non abbiamo passato alcuna opzione durante
la creazione dell'istanza della mappa), tutte le interazioni del mouse e del
tocco sulla mappa sono abilitate e sono disponibili i controlli di zoom e
attribuzione.

Si noti che la chiamata `setView` restituisce anche l'oggetto mappa: la maggior
parte dei metodi `Leaflet` agisce in questo modo quando non restituisce un
valore esplicito, il che consente un comodo concatenamento di metodi simile a
jQuery.

Successivamente, si aggiunge un layer ti tiles alla mappa, in questo caso è un layer OpenStreetMap. La
creazione tile layer comporta l'impostazione del _template URL_ per le tile images, il testo di attribuzione e il
livello massimo di zoom sulla mappa.

```
 //let osm = 'https://tile.openstreetmap.org/{z}/{x}/{y}.png'
 // set base layer
 L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
             maxZoom: 19,
             attribution: '© OpenStreetMap'
 }).addTo(map);
```

Caricando la pagina web che invoca il codice citato sarà possibile vedere la
mappa ed interagire con i controlli base.


### Markers, circles, polygons

Oltre ai layer di tiles, si possono facilmente aggiungere altre funzionalità
inclusi marcatori, poli-linee, poligoni, cerchi e popup. Per aggiungere un
marcatore per esempio:

```
var marker = L.marker([51.5, -0.09]).addTo(map);
```


### Visualizzare le tiles del servzio WSM del Geportale Nazionale del MASE

Ci sono diversi standard per erogare servizi GIS, nel caso specifico
consideriamo:

* **WMS**: è l'abbreviazione di [Web Map
  Service](https://en.wikipedia.org/wiki/Web_Map_Service), è modo diffuso di
  pubblicare mappe da software GIS professionale (e raramente utilizzato da
  non-GISers). Questo formato è simile alle tiles delle mappe, ma più generico
  e non così ben ottimizzato per l'uso nelle mappe web. Un'immagine WMS è
  definita dalle coordinate dei suoi angoli - un'elaborazione che Leaflet opera
  di nascosto.

* **TMS**: [Tiled Map Service](https://en.wikipedia.org/wiki/Tile_Map_Service)
  ed è uno standard di mappatura più focalizzato sulle mappe web, molto simile
  alle tessere delle mappe che Leaflet si aspetta in un `L.TileLayer`.

* **WMTS**: [Web Map Tile
  Service](https://en.wikipedia.org/wiki/Web_Map_Tile_Service), è il protocollo
  standard per i riquadri di mappe e serve i riquadri di mappa direttamente
  utilizzabili in un L.TileLayer.


### WMS con Leaflet

Quando qualcuno pubblica un servizio WMS, molto probabilmente associa ad esso
un documento `GetCapabilities`.

Leaflet non supportae le WMS `GetCapabilities` per cui si deve creare un layer
L.TileLayer.WMS, fornire l'URL WMS di base e specificare le opzioni WMS di cui
si ha bisogno.

Per creare una mappa con le tiles del Geoportale, con Leaflet, si può usare
questo codice:

```javascript
    let wms_url = "http://wms.pcn.minambiente.it/ogc?map=/ms_ogc/WMS_v1.3/raster/DTM_20M.map"
    L.tileLayer.wms(wms_url, {
        layers: "EL.DTM.20M"
    }).addTo(map)
```

va considerato che il concetto di "layer" per Leaflet è un po' diverso di
quello di un servizio WMS.


## Per generare la nuvola LiDAR partendo da uno shape file

Ottenere dati LiDAR (Light Detection and Ranging) corrispondenti a un'area
specifica definita da un file di forma (shapefile) comporta diverse fasi e può
dipendere dalla disponibilità di dataset LiDAR nella regione di interesse. Ecco
una panoramica generale del processo:

1. **Individuare Dataset LiDAR Disponibili**: Innanzitutto, è necessario
   verificare se sono disponibili dataset LiDAR per l'area coperta dal file di
   forma (shapefile). Molte nazioni e regioni hanno portali di dati aperti o
   agenzie governative che forniscono accesso a dataset LiDAR. È possibile
   cercare repository di dati LiDAR o contattare agenzie governative locali,
   organizzazioni ambientali o fornitori di dati GIS per informarsi sulla
   disponibilità dei dati LiDAR.

2. **Comprendere le Specifiche dei Dati LiDAR**: I dataset LiDAR presentano
   diverse specifiche, come densità di punti, formato del punto cloud (LAS,
   LAZ, ecc.), sistema di riferimento delle coordinate e precisione dei
   dati. Assicurarsi di comprendere le specifiche dei dati per valutare se
   soddisfano le proprie esigenze.

3. **Acquisire i Dati LiDAR**: Se sono disponibili dati LiDAR per l'area di
   interesse, sarà necessario ottenere i dataset pertinenti. Alcuni dataset
   potrebbero essere liberamente scaricabili, mentre altri potrebbero
   richiedere richieste di dati, autorizzazioni o accordi di acquisto. Seguire
   le istruzioni del fornitore dei dati per acquisire legalmente i dati.

4. **Tagliare i Dati LiDAR all'Area del Shapefile**: I dataset LiDAR coprono
   spesso aree vaste, e probabilmente sarà necessario ritagliare i dati per
   l'estensione specifica definita dal file di forma (shapefile). Questo
   processo comporta l'uso di software GIS per estrarre i punti LiDAR
   all'interno del confine del file di forma (shapefile).

5. **Convertire i Dati LiDAR nel Formato Desiderato**: I dati LiDAR sono
   generalmente disponibili nei formati LAS o LAZ, ma potrebbe essere
   necessario convertirli in altri formati a seconda dell'applicazione. Alcuni
   software e librerie GIS possono gestire la conversione dei dati LiDAR.

6. **Visualizzare e Elaborare i Dati LiDAR**: Una volta ottenuti i dati LiDAR
   corrispondenti all'area di interesse, è possibile visualizzarli utilizzando
   software di visualizzazione LiDAR specializzati o applicazioni GIS in grado
   di gestire dati di punti cloud. Inoltre, è possibile elaborare ulteriormente
   i dati per specifiche analisi o attività di modellazione.

È importante notare che i dati LiDAR possono essere molto estesi e richiedere
elevate risorse computazionali. Lavorare con dati LiDAR spesso richiede
strumenti specializzati e competenze nel campo del telerilevamento e
dell'analisi di dati geospaziali.  Se ci sono dataset LiDAR specifici
disponibili per la vostra area di interesse, è consigliabile consultare la
documentazione del fornitore dei dati o cercare assistenza da professionisti
GIS che abbiano esperienza nel lavoro con dati LiDAR, per garantire di
elaborare e utilizzare correttamente i dati per lo scopo previsto.

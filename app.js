
var base_layer = {
    "geop": {
        "type": "wsm",
        "url": "http://wms.pcn.minambiente.it/ogc?map=/ms_ogc/WMS_v1.3/raster/de_agostini.map"
    },
    "osm": {
        "type": "def",
        "url": "https://tile.openstreetmap.org/{z}/{x}/{y}.png"
    }
}
var map = null

function show_map(blayer) {
    if (map != null) {
        map.off()
        map.remove()
    }

    // use openstreetmap to display tiles
    map = L.map('map').setView([41.764611, 12.372389], 13);

    bl = base_layer[blayer].type
    switch (bl) {
    case "wsm":
        L.tileLayer.wms(bl.url, {
            layers: "CB.DEAGOSTINI"
        }).addTo(map)
        console.log("mappa base geoportale")
        break

    case "def":
        L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
            maxZoom: 19,
            attribution: '© OpenStreetMap'
        }).addTo(map);
        console.log("mappa base openstreetmap")
        break
    }

    L.tileLayer.wms("http://wms.pcn.minambiente.it/ogc?map=/ms_ogc/WMS_v1.3/raster/DTM_20M.map", {
        layers: "EL.DTM.20M"
    }).addTo(map)
    console.log("mappa LiDAR Geoportale")

    // add the layers
    var drawn_items = new L.FeatureGroup();
    map.addLayer(drawn_items);

    // add a draw control widget on the map
    var drawn_control = new L.Control.Draw({
        draw: {
            polygon: true,
            polyline: false,
            rectangle: false,
            circle: false,
            marker: false
        },
        edit: {
            featureGroup: drawn_items
        }
    });
    map.addControl(drawn_control);

    // handle polygon draw event
    map.on(L.Draw.Event.CREATED, function (event) {
        var layer = event.layer;
        drawn_items.addLayer(layer);

        // get the GeoJSON representation of the drawn polygon
        var geo_JSON_polygon = layer.toGeoJSON();

        // calculate the area (in square meters) using Turf.js
        var area_in_square_meters = turf.area(geo_JSON_polygon);
        alert(`area (sqm): ${Intl.NumberFormat('it-IT', { maximumSignificantDigits: 8 }).format(area_in_square_meters)}`);
    });
}

$(window).bind("load", function() {

    const dd_btn = $("#dropdownMenuButton");

    // Add click event handler to the dropdown items
    $(".dropdown-item").click(function (event) {
        event.preventDefault();
        const sel_item = $(this).text();
        const sel_id = $(this).attr("id");
        // Update the dropdown button text to show the selected item
        dd_btn.text(sel_item);
        show_map(sel_id)
    })
    show_map("osm")
})
